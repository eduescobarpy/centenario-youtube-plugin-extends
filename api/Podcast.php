<?php namespace Opentech\PodcastCentenario\Api;

use Illuminate\Routing\Controller;
use Taema\Youtubegallery\Models\Playlist;

class Podcast extends Controller {

    public function index()
    {
        $playlist = Playlist::whereHas('videos', function($query){
            $query->where('published', true);
        })
        ->with(['latestVideo'])->get();

        return $playlist;
    }

}