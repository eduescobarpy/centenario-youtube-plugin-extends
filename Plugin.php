<?php namespace Opentech\Podcastcentenario;

use System\Classes\PluginBase;
use Taema\Youtubegallery\Models\Video;
use Taema\Youtubegallery\Models\Playlist;

class Plugin extends PluginBase
{
    public $require = [
        'Taema.Youtubegallery',
    ];

    public function boot()
    {
        $this->_extendsYoutubeGallery();
    }

    public function _extendsYoutubeGallery()
    {
        Playlist::extend(function ($model) {

            $model->addDynamicMethod('latestVideo', function () use ($model) {
                $array = $model->videos()->orderBy('created_at', 'DESC');

                return $array;
            });

        });
    }
}
